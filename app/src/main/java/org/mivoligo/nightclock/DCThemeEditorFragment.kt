package org.mivoligo.nightclock

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import org.mivoligo.nightclock.databinding.FragmentDcthemeEditorBinding


class DCThemeEditorFragment : Fragment() {

    private var _binding: FragmentDcthemeEditorBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var listener: OnOwnColorSelected

    interface OnOwnColorSelected {
        fun onOwnColorSelected(isBackColor: Boolean)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        _binding = FragmentDcthemeEditorBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is OnOwnColorSelected) {
            listener = context
        } else {
            throw ClassCastException(context.toString() + " must implement OnOwnColorSelected")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val prefs = PreferenceManager.getDefaultSharedPreferences(activity)
        val isDayMode = prefs[getString(R.string.pref_day_mode_key), false]


        fun setThemePreview(theme: String) {
            if (isDayMode) {
                prefs.edit { put(getString(R.string.pref_day_theme_preview) to theme) }
            } else {
                prefs.edit { put(getString(R.string.pref_night_theme_preview) to theme) }
            }
        }

        fun setForegroundColorPreview(color: String) {
            if (isDayMode) {
                prefs.edit { put(getString(R.string.day_mode_fore_color_preview_key) to color) }
            } else {
                prefs.edit { put(getString(R.string.night_mode_fore_color_preview_key) to color) }
            }
        }

        fun setBackgroundColorPreview(color: String) {
            if (isDayMode) {
                prefs.edit { put(getString(R.string.day_mode_back_color_preview_key) to color) }
            } else {
                prefs.edit { put(getString(R.string.night_mode_back_color_preview_key) to color) }
            }
        }

        binding.rvClockTheme.setHasFixedSize(true)
        binding.rvClockTheme.adapter = DCThemesAdapter(getDCThemes()) {
            setThemePreview(it.themeName)
        }

        binding.rvForegroundColor.setHasFixedSize(true)
        binding.rvForegroundColor.adapter = DCColorsAdapter(getDCColors()) {
            setForegroundColorPreview(it.color)
        }

        binding.rvBackgroundColor.setHasFixedSize(true)
        binding.rvBackgroundColor.adapter = DCColorsAdapter(getDCColors()) {
            setBackgroundColorPreview(it.color)
        }

        binding.btnOwnForeColor.setOnClickListener {
            listener.onOwnColorSelected(false)
        }

        binding.btnOwnBackColor.setOnClickListener {
            listener.onOwnColorSelected(true)
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
