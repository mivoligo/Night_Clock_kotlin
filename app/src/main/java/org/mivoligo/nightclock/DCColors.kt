package org.mivoligo.nightclock


data class DCColors(val color: String)

fun getDCColors(): List<DCColors> {
    return listOf(
            DCColors("#ffffff"),
            DCColors("#9e9e9e"),
            DCColors("#000000"),
            DCColors("#f44336"),
            DCColors("#e91e63"),
            DCColors("#9c27b0"),
            DCColors("#673ab7"),
            DCColors("#3f51b5"),
            DCColors("#2196f3"),
            DCColors("#03a9f4"),
            DCColors("#00bcd4"),
            DCColors("#009688"),
            DCColors("#4caf50"),
            DCColors("#8bc34a"),
            DCColors("#cddc39"),
            DCColors("#ffeb3b"),
            DCColors("#ffc107"),
            DCColors("#ff9800"),
            DCColors("#ff5722"),
            DCColors("#795548"),
            DCColors("#607d8b")

    )
}