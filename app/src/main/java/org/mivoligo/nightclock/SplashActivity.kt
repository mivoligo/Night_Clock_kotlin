package org.mivoligo.nightclock

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.preference.PreferenceManager

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        PreferenceManager.setDefaultValues(this, R.xml.pref_nightclock, false)

        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
