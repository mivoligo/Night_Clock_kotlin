package org.mivoligo.nightclock

import android.app.AlarmManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Handler
import android.service.dreams.DreamService
import android.text.format.DateFormat
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.preference.PreferenceManager
import java.util.*

class ScreensaverDream : DreamService() {

    var moveClock = false
    private val time12Format = "hhmmssa"
    private val time24Format = "HHmmss"
    private var startY = 0f
    private var isDayMode = false
    private var visibleLeadingZero = false
    private var visibleSeconds = false
    private lateinit var foregroundColor: String
    private lateinit var backgroundColor: String
    private val defaultForeColorDay = "#000000"
    private val defaultForeColorNight = "#e91e63"
    private val defaultBackColorDay = "#ffffff"
    private val defaultBackColorNight = "#000000"
    private val defaultThemeDay = "dcV5"
    private val defaultThemeNight = "dcV1"
    private lateinit var selectedThemeList: List<Int>
    private var currentTheme = { key: String -> setDCDigitImage(key, selectedThemeList)}
    lateinit var currentTimeFormat: String
    lateinit var background: View
    private lateinit var dc: View
    lateinit var tvDate: TextView
    private lateinit var tvAlarm: TextView
    lateinit var tvAmPm: TextView
    lateinit var h0: ImageView
    lateinit var h1: ImageView
    lateinit var c0: ImageView
    lateinit var m0: ImageView
    lateinit var m1: ImageView
    lateinit var c1: ImageView
    lateinit var s0: ImageView
    lateinit var s1: ImageView

    private val secHandler: Handler = Handler()



    override fun onAttachedToWindow() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val isFullScreen = prefs[getString(R.string.pref_full_screen_key), resources.getBoolean(R.bool.pref_full_screen_default)]
        super.onAttachedToWindow()

        currentTimeFormat = if (DateFormat.is24HourFormat(applicationContext)) time24Format else time12Format
        isFullscreen = isFullScreen
        setContentView(R.layout.screensaver_main)
        visibleLeadingZero = prefs[getString(R.string.pref_display_leading_zero_key), false]
        visibleSeconds = prefs[getString(R.string.pref_display_seconds_key), resources.getBoolean(R.bool.pref_display_seconds_default)]
        moveClock = prefs[getString(R.string.pref_screen_saver_key), resources.getBoolean(R.bool.pref_screen_saver_default)]

        background = findViewById<View>(R.id.layout_main_sd)
        dc = findViewById<View>(R.id.lt_dc_base)
        tvDate = findViewById(R.id.tv_date)
        tvAlarm = findViewById(R.id.tv_next_alarm)
        tvAmPm = findViewById(R.id.tv_am_pm)
        h0 = findViewById(R.id.iv_h0)
        h1 = findViewById(R.id.iv_h1)
        c0 = findViewById(R.id.iv_c0)
        m0 = findViewById(R.id.iv_m0)
        m1 = findViewById(R.id.iv_m1)
        c1 = findViewById(R.id.iv_c1)
        s0 = findViewById(R.id.iv_s0)
        s1 = findViewById(R.id.iv_s1)
    }

    override fun onDreamingStarted() {
        super.onDreamingStarted()

        registerReceiver(tickReceiver, IntentFilter(Intent.ACTION_TIME_TICK))

        // show seconds if set
        showSeconds(visibleSeconds)
        if (visibleSeconds) startSecondsClock()

        // set position
        startY = calculateStartY()

        dc.animate().y(startY).duration = 500

        // scale down if in screensaver mode
        if (moveClock) scaleClockSize(0.8f)

        //set color
        foregroundColor = readForegroundColor()
        val fColor = Color.parseColor(foregroundColor)

        tvDate.setTextColor(fColor)
        tvAlarm.setTextColor(fColor)
        tvAmPm.setTextColor(fColor)
        h0.setColorFilter(fColor)
        h1.setColorFilter(fColor)
        c0.setColorFilter(fColor)
        m0.setColorFilter(fColor)
        m1.setColorFilter(fColor)
        c1.setColorFilter(fColor)
        s0.setColorFilter(fColor)
        s1.setColorFilter(fColor)
        setTextViewDrawableColor(tvAlarm, foregroundColor)


        backgroundColor = readBackgroundColor()

        background.setBackgroundColor(Color.parseColor(backgroundColor))

        // set theme and clock
        selectedThemeList = when (readTheme()) {
            "dcV1" -> dcV1List
            "dcV2" -> dcV2List
            "dcV3" -> dcV3List
            "dcV4" -> dcV4List
            "dcV5" -> dcV5List
            "dcV6" -> dcV6List
            "dcV7" -> dcV7List
            else -> dcV1List
        }
        setDigitalClock(h0, h1, c0, m0, m1, c1, s0, s1, tvAmPm, currentTimeFormat, currentTheme)

        // show date
        setDate(tvDate)

        // show alarm
        setNextAlarm(tvAlarm)


    }

    override fun onDreamingStopped() {
        super.onDreamingStopped()
        unregisterReceiver(tickReceiver)
        secHandler.removeCallbacksAndMessages(null)
    }

    private fun calculateStartY(): Float {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val isLandscape = this.resources.getBoolean(R.bool.is_landscape)
        val oldStartY = this.resources.getDimension(R.dimen.dp16)
        return if (isLandscape) {
            prefs[getString(R.string.pref_start_y_land), oldStartY]
        } else {
            prefs[getString(R.string.pref_start_y_port), oldStartY]
        }
    }

    private fun readForegroundColor(): String {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        isDayMode = prefs[getString(R.string.pref_day_mode_key), false]
        return if (isDayMode) {
            prefs[getString(R.string.pref_day_mode_fore_color), defaultForeColorDay]
        } else {
            prefs[getString(R.string.pref_night_mode_fore_color), defaultForeColorNight]
        }
    }

    private fun readBackgroundColor(): String {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        isDayMode = prefs[getString(R.string.pref_day_mode_key), false]
        return if (isDayMode) {
            prefs[getString(R.string.pref_day_mode_back_color), defaultBackColorDay]
        } else {
            prefs[getString(R.string.pref_night_mode_back_color), defaultBackColorNight]
        }
    }

    private fun readTheme(): String {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        isDayMode = prefs[getString(R.string.pref_day_mode_key), false]
        return if (isDayMode) {
            prefs[getString(R.string.pref_day_theme), defaultThemeDay]
        } else {
            prefs[getString(R.string.pref_night_theme), defaultThemeNight]
        }
    }

    private fun setDate(tvDate: TextView){
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val currentDateFormat = prefs[getString(R.string.pref_date_format_key), "d MMMM yyyy"]
        val isDateVisible =  prefs[getString(R.string.pref_display_date_key), true]
        if (isDateVisible) tvDate.text = formatTimeAndDate("EEEE\n$currentDateFormat")
    }

    private fun setNextAlarm(tvAlarm: TextView) {

        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val isAlarmVisible = prefs[getString(R.string.pref_display_alarms_key), resources.getBoolean(R.bool.pref_display_alarms_default)]
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val clockInfo: AlarmManager.AlarmClockInfo? = alarmManager.nextAlarmClock
        val nextAlarmTime = clockInfo?.triggerTime

        if (isAlarmVisible) {
            if (nextAlarmTime != null) {
                val nextAlarmDate = Date(nextAlarmTime)
                val dayOfNextAlarm = DateFormat.format("EEE", nextAlarmDate)
                val timeFormat = DateFormat.getTimeFormat(applicationContext)
                val timeOfNextAlarm = timeFormat.format(nextAlarmDate)

                tvAlarm.text = "$dayOfNextAlarm $timeOfNextAlarm"
            } else {
                tvAlarm.text = getString(R.string.no_alarms)
            }
        } else {
            tvAlarm.visibility = View.INVISIBLE
        }
    }




    // Set Digital Clock
    private fun setDigitalClock(h0: ImageView, h1: ImageView, c0: ImageView, m0: ImageView, m1: ImageView, c1: ImageView, s0: ImageView, s1: ImageView,  am_pm: TextView, currentTimeFormat: String, currentDCTheme: (String) -> (Int) ) {
        val currentTimeString = formatTimeAndDate(currentTimeFormat)

        if (!visibleLeadingZero && currentTimeString.substring(0, 1) == "0") {
            h0.visibility = View.INVISIBLE
        } else {
            h0.visibility = View.VISIBLE
        }
        h0.setImageResource(currentDCTheme(currentTimeString.substring(0, 1)))
        h1.setImageResource(currentDCTheme(currentTimeString.substring(1, 2)))
        c0.setImageResource(currentDCTheme(":"))
        m0.setImageResource(currentDCTheme(currentTimeString.substring(2, 3)))
        m1.setImageResource(currentDCTheme(currentTimeString.substring(3, 4)))
        c1.setImageResource(currentDCTheme(":"))
        s0.setImageResource(currentDCTheme(currentTimeString.substring(4, 5)))
        s1.setImageResource(currentDCTheme(currentTimeString.substring(5, 6)))

        am_pm.text = currentTimeString.substring(6)
    }

     // Update time every minute
    private val tickReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.action == Intent.ACTION_TIME_TICK) {
                setDigitalClock(h0, h1, c0, m0, m1, c1, s0, s1, tvAmPm, currentTimeFormat, currentTheme)
                setDate(tvDate)
                if (moveClock) moveClockScreenSaver(0.8f)
            }
        }
    }

    fun moveClockScreenSaver(scale: Float) {
        val viewX = dc.x
        val viewY = dc.y
        val isLandscape = this.resources.getBoolean(R.bool.is_landscape)
        val movingClockHeight = if (isLandscape) {
            dc.height * scale + tvDate.height
        } else {
            dc.height * scale + tvDate.height + tvAlarm.height
        }
        val x1 = 0f
        val x2 = (background.width - dc.width * scale) / 2f
        val x3 = background.width - dc.width * scale
        val y1 = resources.getDimension(R.dimen.dp16)
        val y2 = background.height / 3f - movingClockHeight / 2f
        val y3 = (background.height - movingClockHeight) / 2f
        val y4 = 2 * background.height / 3f - movingClockHeight / 2f
        val y5 = background.height - movingClockHeight
//         2 | 10 | 15
//         5 | 13 |  3
//         8 |  1 |  6
//        11 |  4 |  9
//        14 |  7 | 12
        fun setXY(x: Float, y: Float) {
            dc.x = x
            dc.y = y
        }
        when {
            viewX == x2 && viewY == y3 -> setXY(x1, y1) // 2
            viewX == x1 && viewY == y1 -> setXY(x3, y2) // 3
            viewX == x3 && viewY == y2 -> setXY(x2, y4) // 4
            viewX == x2 && viewY == y4 -> setXY(x1, y2) // 5
            viewX == x1 && viewY == y2 -> setXY(x3, y3) // 6
            viewX == x3 && viewY == y3 -> setXY(x2, y5) // 7
            viewX == x2 && viewY == y5 -> setXY(x1, y3) // 8
            viewX == x1 && viewY == y3 -> setXY(x3, y4) // 9
            viewX == x3 && viewY == y4 -> setXY(x2, y1) // 10
            viewX == x2 && viewY == y1 -> setXY(x1, y4) // 11
            viewX == x1 && viewY == y4 -> setXY(x3, y5) // 12
            viewX == x3 && viewY == y5 -> setXY(x2, y2) // 13
            viewX == x2 && viewY == y2 -> setXY(x1, y5) // 14
            viewX == x1 && viewY == y5 -> setXY(x3, y1) // 15
            else -> setXY(x2, y3) // 1
        }
    }

    private fun scaleClockSize(scale: Float) {
        with(dc) {
            pivotX = 0f
            pivotY = 0f
            scaleX = scale
            scaleY = scale
        }
    }

    private fun startSecondsClock() {
        val runnable = object : Runnable {
            override fun run() {
                val delay = 1000 - System.currentTimeMillis() % 1000
                setDigitalClock(h0, h1, c0, m0, m1, c1, s0, s1, tvAmPm, currentTimeFormat, currentTheme)
                secHandler.postDelayed(this, delay)
            }
        }

        secHandler.postDelayed(runnable, 1000)
    }

    private fun showSeconds(isSecondsVisible: Boolean) {
        if (isSecondsVisible) {
            c1.visibility = View.VISIBLE
            s0.visibility = View.VISIBLE
            s1.visibility = View.VISIBLE
        } else {
            c1.visibility = View.GONE
            s0.visibility = View.GONE
            s1.visibility = View.GONE
        }
    }
}
