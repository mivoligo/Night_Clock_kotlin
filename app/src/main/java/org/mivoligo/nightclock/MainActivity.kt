package org.mivoligo.nightclock

import android.annotation.TargetApi
import android.app.AlertDialog
import android.content.*
import android.content.res.ColorStateList
import android.graphics.Color
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.provider.AlarmClock
import android.provider.Settings
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import android.text.format.DateFormat
import android.view.*
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.digital_clock_base.*
import kotlinx.android.synthetic.main.fragment_digital_clock.*
import android.content.pm.ActivityInfo
import android.app.AlarmManager
import android.util.Log
import androidx.preference.PreferenceManager
import org.mivoligo.nightclock.databinding.ActivityMainBinding
import java.util.*


class MainActivity : AppCompatActivity(), View.OnTouchListener, SharedPreferences.OnSharedPreferenceChangeListener, DCThemeEditorFragment.OnOwnColorSelected {

    private lateinit var binding: ActivityMainBinding

    override fun onSharedPreferenceChanged(prefs: SharedPreferences, key: String) {
        when (key) {
            getString(R.string.night_mode_fore_color_preview_key) -> {
                foregroundColorNightPreview = prefs[getString(R.string.night_mode_fore_color_preview_key), ""]
                setForegroundClockColor(foregroundColorNightPreview)
            }
            getString(R.string.night_mode_back_color_preview_key) -> {
                backgroundColorNightPreview = prefs[getString(R.string.night_mode_back_color_preview_key), ""]
                setDCThemeEditBackColorPreview(backgroundColorNightPreview)
            }

            getString(R.string.pref_night_theme_preview) -> {
                themeNightPreview = prefs[getString(R.string.pref_night_theme_preview), ""]
                val currentTimeFormat = if (DateFormat.is24HourFormat(applicationContext)) time24Format else time12Format
                selectedThemeNightList = when (themeNightPreview) {
                    "dcV1" -> dcV1List
                    "dcV2" -> dcV2List
                    "dcV3" -> dcV3List
                    "dcV4" -> dcV4List
                    "dcV5" -> dcV5List
                    "dcV6" -> dcV6List
                    "dcV7" -> dcV7List
                    else -> dcV1List
                }
                setDigitalClock(currentTimeFormat, currentThemeNight)
            }

            getString(R.string.day_mode_fore_color_preview_key) -> {
                foregroundColorDayPreview = prefs[getString(R.string.day_mode_fore_color_preview_key), ""]
                setForegroundClockColor(foregroundColorDayPreview)
            }
            getString(R.string.day_mode_back_color_preview_key) -> {
                backgroundColorDayPreview = prefs[getString(R.string.day_mode_back_color_preview_key), ""]
                setDCThemeEditBackColorPreview(backgroundColorDayPreview)
            }

            getString(R.string.pref_day_theme_preview) -> {
                themeDayPreview = prefs[getString(R.string.pref_day_theme_preview), ""]
                val currentTimeFormat = if (DateFormat.is24HourFormat(applicationContext)) time24Format else time12Format
                selectedThemeDayList = when (themeDayPreview) {
                    "dcV1" -> dcV1List
                    "dcV2" -> dcV2List
                    "dcV3" -> dcV3List
                    "dcV4" -> dcV4List
                    "dcV5" -> dcV5List
                    "dcV6" -> dcV6List
                    "dcV7" -> dcV7List
                    else -> dcV1List
                }
                setDigitalClock(currentTimeFormat, currentThemeDay)
            }

            getString(R.string.pref_night_mode_fore_color) -> {
                foregroundColorNight = prefs[getString(R.string.pref_night_mode_fore_color), ""]
                setDateColor(foregroundColorNight)
                setAlarmColor(foregroundColorNight)
            }
            getString(R.string.pref_night_mode_back_color) -> backgroundColorNight = prefs[getString(R.string.pref_night_mode_back_color), ""]
            getString(R.string.pref_night_theme) -> selectedThemeNight = prefs[getString(R.string.pref_night_theme), ""]

            getString(R.string.pref_day_mode_fore_color) -> {
                foregroundColorDay = prefs[getString(R.string.pref_day_mode_fore_color), ""]
                setDateColor(foregroundColorDay)
                setAlarmColor(foregroundColorDay)
            }
            getString(R.string.pref_day_mode_back_color) -> backgroundColorDay = prefs[getString(R.string.pref_day_mode_back_color), ""]
            getString(R.string.pref_day_theme) -> selectedThemeDay = prefs[getString(R.string.pref_day_theme), ""]

            getString(R.string.pref_display_leading_zero_key) -> visibleLeadingZero = prefs[getString(R.string.pref_display_leading_zero_key), false]
        }
    }


    private val maxClickDuration = 300
    private var startClickTime: Long = 0
    private var dX = 0f
    private var dY = 0f

    private val screenSaverClockScale = 0.8f

    private var visibleButtons = false
    private var visibleFABMenu = false
    private var visibleBrightnessBar = false

    private val codeWriteSettingsPermissionBrightness = 1
    private val codeWriteSettingsPermissionOrientation = 2
    private val handler: Handler = Handler()
    private val secHandler: Handler = Handler()
    private val runnable = object : Runnable {
        override fun run() {
            val delay = 1000 - System.currentTimeMillis() % 1000
            val currentTimeFormat = if (DateFormat.is24HourFormat(applicationContext)) time24Format else time12Format
            val currentDCTheme = if (isDayMode) currentThemeDay else currentThemeNight
            setDigitalClock(currentTimeFormat, currentDCTheme)
            secHandler.postDelayed(this, delay)
        }
    }

    private var systemBrightnessValue = 50
    private var systemBrightnessMode = 0
    private var systemAutoRotate = 0

    private val time12Format = "hhmmssa"
    private val time24Format = "HHmmss"

    private var visibleLeadingZero = false

    var isDayMode = false
    var isDCThemeEdited = false
    private var isOwnColorEdited = false
    private lateinit var ownColorEditType: String


    private val defaultForeColorDay = "#000000"
    private val defaultBackColorDay = "#ffffff"
    private val defaultThemeDay = "dcV5"

    private val defaultForeColorNight = "#e91e63"
    private val defaultBackColorNight = "#000000"
    private val defaultThemeNight = "dcV1"

    private lateinit var selectedThemeNightList: List<Int>
    var currentThemeNight = { key: String -> setDCDigitImage(key, selectedThemeNightList) }
    private lateinit var selectedThemeDayList: List<Int>
    var currentThemeDay = { key: String -> setDCDigitImage(key, selectedThemeDayList) }

    private lateinit var foregroundColorDay: String
    private lateinit var backgroundColorDay: String
    private lateinit var foregroundColorDayPreview: String
    private lateinit var backgroundColorDayPreview: String
    private lateinit var selectedThemeDay: String
    private lateinit var selectedThemeNight: String
    private lateinit var themeDayPreview: String
    private lateinit var foregroundColorNight: String
    private lateinit var backgroundColorNight: String
    private lateinit var foregroundColorNightPreview: String
    private lateinit var backgroundColorNightPreview: String
    private lateinit var themeNightPreview: String
    private val editorFragment = DCThemeEditorFragment()
    private val ownColorFragment = OwnColorFragment()
    private val bundle = Bundle()

    override fun onOwnColorSelected(isBackColor: Boolean) {
        val color = when {
            isDayMode && isBackColor -> backgroundColorDayPreview
            isDayMode && !isBackColor -> foregroundColorDayPreview
            !isDayMode && isBackColor -> backgroundColorNightPreview
            !isDayMode && !isBackColor -> foregroundColorNightPreview
            else -> "#000000"
        }
        ownColorEditType = when {
            isBackColor -> "backcolor"
            else -> "forecolor"
        }
        bundle.putString("selected_color", color)
        bundle.putBoolean("isBackColor", isBackColor)
        ownColorFragment.arguments = bundle
        addOwnColorFragment()
        isOwnColorEdited = true
    }

    // Update time every minute
    private val tickReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            val isDateVisible = prefs[getString(R.string.pref_display_date_key), resources.getBoolean(R.bool.pref_display_date_default)]
            val isAlarmVisible = prefs[getString(R.string.pref_display_alarms_key), resources.getBoolean(R.bool.pref_display_alarms_default)]
            val isScreenSaver = prefs[getString(R.string.pref_screen_saver_key), resources.getBoolean(R.bool.pref_screen_saver_default)]
            val currentTimeFormat = if (DateFormat.is24HourFormat(applicationContext)) time24Format else time12Format
            val currentDateFormat = prefs[getString(R.string.pref_date_format_key), ""]
            if (isScreenSaver && !isDCThemeEdited) moveClockScreenSaver(screenSaverClockScale)
            if (intent?.action == Intent.ACTION_TIME_TICK) {
                if (isDayMode) {
                    setDigitalClock(currentTimeFormat, currentThemeDay)
                } else {
                    setDigitalClock(currentTimeFormat, currentThemeNight)
                }
                if (!isDCThemeEdited) showAlarms(isAlarmVisible)
                if (isAlarmVisible) tv_next_alarm.text = checkForNextAlarm()
                if (isDateVisible) tv_date.text = setDate(currentDateFormat)
            }
        }
    }

    // Start seconds clock
    private fun startSecondsClock() = secHandler.postDelayed(runnable, 1000)

    // Stop seconds clock
    private fun stopSecondsClock() = secHandler.removeCallbacksAndMessages(null)

    // Set Digital Clock
    fun setDigitalClock(currentTimeFormat: String, currentDCTheme: (String) -> (Int)) {
        val currentTimeString = formatTimeAndDate(currentTimeFormat)

        if (!visibleLeadingZero && currentTimeString.substring(0, 1) == "0") {
            iv_h0.visibility = View.INVISIBLE
        } else {
            iv_h0.visibility = View.VISIBLE
        }
        iv_h0.setImageResource(currentDCTheme(currentTimeString.substring(0, 1)))
        iv_h1.setImageResource(currentDCTheme(currentTimeString.substring(1, 2)))
        iv_c0.setImageResource(currentDCTheme(":"))
        iv_m0.setImageResource(currentDCTheme(currentTimeString.substring(2, 3)))
        iv_m1.setImageResource(currentDCTheme(currentTimeString.substring(3, 4)))
        iv_c1.setImageResource(currentDCTheme(":"))
        iv_s0.setImageResource(currentDCTheme(currentTimeString.substring(4, 5)))
        iv_s1.setImageResource(currentDCTheme(currentTimeString.substring(5, 6)))

        tv_am_pm.text = currentTimeString.substring(6)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)

        val isDateVisible = prefs[getString(R.string.pref_display_date_key), resources.getBoolean(R.bool.pref_display_date_default)]
        val isAlarmVisible = prefs[getString(R.string.pref_display_alarms_key), resources.getBoolean(R.bool.pref_display_alarms_default)]
        val currentTimeFormat = if (DateFormat.is24HourFormat(applicationContext)) time24Format else time12Format
        val currentDateFormat = prefs[getString(R.string.pref_date_format_key), ""]
        isDayMode = prefs[getString(R.string.pref_day_mode_key), false]
        foregroundColorDay = prefs[getString(R.string.pref_day_mode_fore_color), defaultForeColorDay]
        backgroundColorDay = prefs[getString(R.string.pref_day_mode_back_color), defaultBackColorDay]
        selectedThemeDay = prefs[getString(R.string.pref_day_theme), defaultThemeDay]
        selectedThemeDayList = when (selectedThemeDay) {
            "dcV1" -> dcV1List
            "dcV2" -> dcV2List
            "dcV3" -> dcV3List
            "dcV4" -> dcV4List
            "dcV5" -> dcV5List
            "dcV6" -> dcV6List
            "dcV7" -> dcV7List
            else -> dcV1List
        }
        foregroundColorDayPreview = prefs[getString(R.string.day_mode_fore_color_preview_key), foregroundColorDay]
        backgroundColorDayPreview = prefs[getString(R.string.day_mode_back_color_preview_key), backgroundColorDay]
        themeDayPreview = prefs[getString(R.string.pref_day_theme_preview), selectedThemeDay]

        foregroundColorNight = prefs[getString(R.string.pref_night_mode_fore_color), defaultForeColorNight]
        backgroundColorNight = prefs[getString(R.string.pref_night_mode_back_color), defaultBackColorNight]
        selectedThemeNight = prefs[getString(R.string.pref_night_theme), defaultThemeNight]
        selectedThemeNightList = when (selectedThemeNight) {
            "dcV1" -> dcV1List
            "dcV2" -> dcV2List
            "dcV3" -> dcV3List
            "dcV4" -> dcV4List
            "dcV5" -> dcV5List
            "dcV6" -> dcV6List
            "dcV7" -> dcV7List
            else -> dcV1List
        }
        foregroundColorNightPreview = prefs[getString(R.string.night_mode_fore_color_preview_key), foregroundColorNight]
        backgroundColorNightPreview = prefs[getString(R.string.night_mode_back_color_preview_key), backgroundColorNight]
        themeNightPreview = prefs[getString(R.string.pref_night_theme_preview), selectedThemeNight]
        visibleLeadingZero = prefs[getString(R.string.pref_display_leading_zero_key), false]

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        binding.toolbar.visibility = View.INVISIBLE

        showDate(isDateVisible)
        if (isDateVisible) tv_date.text = setDate(currentDateFormat)
        showAlarms(isAlarmVisible)
        if (isAlarmVisible) tv_next_alarm.text = checkForNextAlarm()
        if (isDayMode) {
            setDigitalClock(currentTimeFormat, currentThemeDay)
            setDayMode()
        } else {
            setDigitalClock(currentTimeFormat, currentThemeNight)
            setNightMode()
        }
        registerReceiver(tickReceiver, IntentFilter(Intent.ACTION_TIME_TICK))
       binding.root.setOnClickListener {
            if (!isDCThemeEdited) {
                if (visibleButtons) hideButtons() else showButtons()
            }
        }
        lt_dc_base.setOnClickListener {
            if (!isDCThemeEdited) {
                if (visibleButtons) hideButtons() else showButtons()
            }
        }
        tv_next_alarm.setOnClickListener { openAlarms() }
        binding.vSemiTransBack.setOnClickListener {
            when {
                visibleBrightnessBar -> hideBrightnessSeekBar()
                visibleFABMenu -> hideFABMenu()
            }
        }
        binding.fabMain.setOnClickListener { if (!visibleFABMenu) showFABMenu() else hideFABMenu() }
        binding.llFab1Menu.setOnClickListener {
            if (isDayMode) {
                setNightMode()
                setBrightnessValue()
                setDigitalClock(currentTimeFormat, currentThemeNight)
            } else {
                setDayMode()
                setBrightnessValue()
                setDigitalClock(currentTimeFormat, currentThemeDay)
            }
            prefs.edit { put(getString(R.string.pref_day_mode_key) to isDayMode) }
        }
        binding.llFab2Menu.setOnClickListener { showDCThemeEditor() }

        binding.llFab3Menu.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val returnValue = Settings.System.canWrite(this)
                if (returnValue) {
                    showBrightnessSeekBar()
                } else {
                    showAskForPermissionDialog("brightness")
                }
            } else {
                showBrightnessSeekBar()
            }
        }

        binding.llFab4Menu.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val returnValue = Settings.System.canWrite(this)
                if (returnValue) {
                    showOrientationSelectionDialog()
                } else {
                    showAskForPermissionDialog("screen orientation")
                }
            } else {
                showOrientationSelectionDialog()
            }
        }
        binding.btnResetBrightness.setOnClickListener {
            resetBrightnessSettings()
            hideResetBrightnessButton()
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        val currentTimeFormat = if (DateFormat.is24HourFormat(applicationContext)) time24Format else time12Format

        super.onWindowFocusChanged(hasFocus)

        if (isDayMode) {
            setTextViewDrawableColor(tv_next_alarm, foregroundColorDay)
            setDigitalClock(currentTimeFormat, currentThemeDay)
        } else {
            setTextViewDrawableColor(tv_next_alarm, foregroundColorNight)
            setDigitalClock(currentTimeFormat, currentThemeNight)
        }
    }

    private fun setFullScreenOrNot(context: Context) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val isFullScreen = prefs[getString(R.string.pref_full_screen_key), resources.getBoolean(R.bool.pref_full_screen_default)]
        if (isFullScreen) setFullScreenFlags() else cancelFullScreenFlags()
    }

    override fun onResume() {
        systemBrightnessMode = Settings.System.getInt(contentResolver, Settings.System.SCREEN_BRIGHTNESS_MODE)
        systemBrightnessValue = Settings.System.getInt(contentResolver, Settings.System.SCREEN_BRIGHTNESS)
        systemAutoRotate = Settings.System.getInt(contentResolver, Settings.System.ACCELEROMETER_ROTATION)

        super.onResume()

        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val isSecondsVisible = prefs[getString(R.string.pref_display_seconds_key), resources.getBoolean(R.bool.pref_display_seconds_default)]
        val isScreenSaver = prefs[getString(R.string.pref_screen_saver_key), resources.getBoolean(R.bool.pref_screen_saver_default)]
        val isDateVisible = prefs[getString(R.string.pref_display_date_key), resources.getBoolean(R.bool.pref_display_date_default)]
        val isAlarmVisible = prefs[getString(R.string.pref_display_alarms_key), resources.getBoolean(R.bool.pref_display_alarms_default)]
        val currentDateFormat = prefs[getString(R.string.pref_date_format_key), ""]
        prefs.registerOnSharedPreferenceChangeListener(this)
        setBrightnessValue()
        setScreenRotationOnStart()
        setFullScreenOrNot(this)
        lt_dc_base.x = 0f
        lt_dc_base.y = getStartY()
        visibleLeadingZero = prefs[getString(R.string.pref_display_leading_zero_key), false]
        showSeconds(isSecondsVisible)
        if (isSecondsVisible) startSecondsClock() else stopSecondsClock()

        if (isScreenSaver) {
            lt_dc_base.setOnTouchListener(null)
            scaleClockSize(screenSaverClockScale)
        } else {
            scaleClockSize(1.0f)
            lt_dc_base.setOnTouchListener(this)
        }
        if (!isDCThemeEdited) showDate(isDateVisible)
        if (isDateVisible) tv_date.text = setDate(currentDateFormat)
        if (!isDCThemeEdited) showAlarms(isAlarmVisible)
        if (isAlarmVisible) tv_next_alarm.text = checkForNextAlarm()
    }

    override fun onPause() {
        if (isOwnColorEdited) {
            removeOwnColorFragment()
            restoreDCForegroundPreviewColor()
            restoreDCBackgroundPreviewColor()
        }
        if (isDCThemeEdited) {
            hideDCThemeEditor()
            restoreDCForegroundPreviewColor()
            restoreDCBackgroundPreviewColor()
            lt_dc_base.setBackgroundColor(Color.TRANSPARENT)
            restoreThemePreview()
        }
        showSeconds(false)
        stopSecondsClock()
        super.onPause()
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        restoreSystemBrightness(systemBrightnessValue)
        restoreSystemAutoRotation(systemAutoRotate)
        prefs.unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun onDestroy() {

        super.onDestroy()
        unregisterReceiver(tickReceiver)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        if (isDCThemeEdited) {
            menu?.removeItem(R.id.action_settings)
        } else {
            menu?.removeItem(R.id.action_save_theme)
        }
        return super.onPrepareOptionsMenu(menu)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
            }
            R.id.action_save_theme -> {
                if (isOwnColorEdited) {
                    removeOwnColorFragment()
                } else {
                    saveDCForegroundColor()
                    saveDCBackgroundColor()
                    saveDCTheme()
                    hideDCThemeEditor()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        when {
            isOwnColorEdited -> {
                restoreDCForegroundPreviewColor()
                restoreDCBackgroundPreviewColor()
                removeOwnColorFragment()
            }
            isDCThemeEdited -> {
                hideDCThemeEditor()
                restoreDCForegroundPreviewColor()
                restoreDCBackgroundPreviewColor()
                lt_dc_base.setBackgroundColor(Color.TRANSPARENT)
                restoreThemePreview()
            }
            visibleBrightnessBar -> hideBrightnessSeekBar()
            visibleFABMenu -> hideFABMenu()
            visibleButtons -> hideButtons()
            else -> super.onBackPressed()
        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {

        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                startClickTime = event.downTime
                dX = v!!.x - event.rawX
                dY = v.y - event.rawY
            }
            MotionEvent.ACTION_MOVE -> {
                var newX = event.rawX + dX
                var newY = event.rawY + dY

                when {
                    newX <= 0 -> newX = 0f
                    newX >= binding.root.width - v!!.width ->
                        newX = (binding.root.width - v.width).toFloat()
                }
                when {
                    newY <= 0 -> newY = 0f
                    newY >= binding.root.height - v!!.height ->
                        newY = (binding.root.height - v.height).toFloat()
                }
                v!!.animate()
                        .x(newX)
                        .y(newY)
                        .duration = 0
            }
            MotionEvent.ACTION_UP -> {
                setStartY(v!!.y)
                val clickDuration = event.eventTime - startClickTime
                if (clickDuration < maxClickDuration) v.performClick()
            }

        }
        return true
    }

    private fun checkForNextAlarm(): String {


        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val clockInfo: AlarmManager.AlarmClockInfo? = alarmManager.nextAlarmClock
        val nextAlarmTime = clockInfo?.triggerTime

        return if (nextAlarmTime != null) {
            val nextAlarmDate = Date(nextAlarmTime)
            val dayOfNextAlarm = DateFormat.format("EEE", nextAlarmDate)
            val timeFormat = DateFormat.getTimeFormat(applicationContext)
            val timeOfNextAlarm = timeFormat.format(nextAlarmDate)

            "$dayOfNextAlarm $timeOfNextAlarm"
        } else {
            getString(R.string.no_alarms)
        }
    }


    private fun showButtons() {

        binding.fabMain.show()
        binding.toolbar.visibility = View.VISIBLE
        visibleButtons = true

        handler.postDelayed({ hideButtons() }, 3000)
    }

    private fun hideButtons() {

        handler.removeCallbacksAndMessages(null)
        binding.fabMain.hide()
        binding.toolbar.visibility = View.INVISIBLE
        visibleButtons = false
    }

    private fun showFABMenu() {
        binding.vSemiTransBack.visibility = View.VISIBLE
        binding.llFab1Menu.visibility = View.VISIBLE
        binding.llFab2Menu.visibility = View.VISIBLE
        binding.llFab3Menu.visibility = View.VISIBLE
        binding.llFab4Menu.visibility = View.VISIBLE
        binding.llFab1Menu.animate().translationY(-resources.getDimension(R.dimen.dp72)).alpha(1f).duration = 50
        binding.llFab2Menu.animate().translationY(-resources.getDimension(R.dimen.dp128)).alpha(1f).duration = 150
        binding.llFab3Menu.animate().translationY(-resources.getDimension(R.dimen.dp184)).alpha(1f).duration = 250
        binding.llFab4Menu.animate().translationY(-resources.getDimension(R.dimen.dp240)).alpha(1f).duration = 350
        binding.fabMain.animate().rotationBy(90f)
        binding.fabMain.setImageResource(R.drawable.ic_close_24dp)
        handler.removeCallbacksAndMessages(null)
        visibleFABMenu = true
    }

    private fun hideFABMenu() {
        binding.llFab1Menu.animate().translationY(0f).alpha(0f).duration = 350
        binding.llFab2Menu.animate().translationY(0f).alpha(0f).duration = 250
        binding.llFab3Menu.animate().translationY(0f).alpha(0f).duration = 150
        binding.llFab4Menu.animate().translationY(0f).alpha(0f).duration = 50
        binding.fabMain.animate().rotationBy(-90f)
        binding.fabMain.setImageResource(R.drawable.ic_edit_24dp)
        binding.vSemiTransBack.visibility = View.INVISIBLE
        handler.postDelayed({
            binding.llFab1Menu.visibility = View.GONE
            binding.llFab2Menu.visibility = View.GONE
            binding.llFab3Menu.visibility = View.GONE
            binding.llFab4Menu.visibility = View.GONE
        }, 400)
        visibleFABMenu = false
        handler.postDelayed({ hideButtons() }, 3000)
    }

    private fun showBrightnessSeekBar() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        binding.sbScreenBrightness.visibility = View.VISIBLE
        binding.sbScreenBrightness.animate().alpha(1f)
        when {
            isDayMode -> when {
                prefs.contains(getString(R.string.day_mode_brightness_value)) -> showResetBrightnessButton()
            }
            else -> when {
                prefs.contains(getString(R.string.night_mode_brightness_value)) -> showResetBrightnessButton()
            }
        }
        binding.vSemiTransBack.animate().alpha(0f)
        binding.toolbar.visibility = View.INVISIBLE
        restoreDefaultStateForButtons()
        setCurrentBrightness()
        visibleBrightnessBar = true
        handler.postDelayed({ hideBrightnessSeekBar() }, 5000)
    }

    private fun hideBrightnessSeekBar() {
        handler.removeCallbacksAndMessages(null)
        binding.sbScreenBrightness.animate().alpha(0f)
        binding.sbScreenBrightness.visibility = View.GONE
        hideResetBrightnessButton()
        binding.vSemiTransBack.alpha = 0.5f
        binding.vSemiTransBack.visibility = View.INVISIBLE
        visibleBrightnessBar = false
    }

    private fun setCurrentBrightness() {
        // To handle the auto
        Settings.System.putInt(contentResolver,
                Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL)
        var brightness = Settings.System.getInt(contentResolver, Settings.System.SCREEN_BRIGHTNESS)

        binding.sbScreenBrightness.progress = brightness

        binding.sbScreenBrightness.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                brightness = progress
                //Set the system brightness using the brightness variable value
                Settings.System.putInt(contentResolver, Settings.System.SCREEN_BRIGHTNESS, brightness)
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
                handler.removeCallbacksAndMessages(null)
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
                saveBrightnessValue(binding.sbScreenBrightness.progress)
                showResetBrightnessButton()
                handler.postDelayed({ hideBrightnessSeekBar() }, 4000)
            }
        }

        )
    }

    fun saveBrightnessValue(value: Int) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        if (isDayMode) {
            prefs.edit { put(getString(R.string.day_mode_brightness_value) to value) }
        } else {
            prefs.edit { put(getString(R.string.night_mode_brightness_value) to value) }
        }
    }

    private fun setBrightnessValue() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val brightness = if (isDayMode) {
            prefs[getString(R.string.day_mode_brightness_value), systemBrightnessValue]
        } else {
            prefs[getString(R.string.night_mode_brightness_value), systemBrightnessValue]
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val returnValue = Settings.System.canWrite(this)
            if (returnValue) {
                Settings.System.putInt(contentResolver,
                        Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL)
                Settings.System.putInt(contentResolver, Settings.System.SCREEN_BRIGHTNESS, brightness)
            }
        } else {
            Settings.System.putInt(contentResolver,
                    Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL)
            Settings.System.putInt(contentResolver, Settings.System.SCREEN_BRIGHTNESS, brightness)
        }
    }

    private fun restoreSystemBrightness(brightness: Int) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val returnValue = Settings.System.canWrite(this)
            if (returnValue) {
                Settings.System.putInt(contentResolver, Settings.System.SCREEN_BRIGHTNESS, brightness)
                if (systemBrightnessMode == 1) {
                    Settings.System.putInt(contentResolver,
                            Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC)
                }
            }
        } else {
            Settings.System.putInt(contentResolver, Settings.System.SCREEN_BRIGHTNESS, brightness)
            if (systemBrightnessMode == 1) {
                Settings.System.putInt(contentResolver,
                        Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC)
            }

        }
    }

    private fun resetBrightnessSettings() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        restoreSystemBrightness(systemBrightnessValue)
        binding.sbScreenBrightness.progress = systemBrightnessValue
        if (isDayMode) {
            prefs.edit().remove(getString(R.string.day_mode_brightness_value)).apply()
        } else {
            prefs.edit().remove(getString(R.string.night_mode_brightness_value)).apply()
        }
    }

    fun showResetBrightnessButton() {
        binding.btnResetBrightness.visibility = View.VISIBLE
        binding.btnResetBrightness.animate().alpha(1f)
    }

    private fun hideResetBrightnessButton() {
        binding.btnResetBrightness.animate().alpha(0f)
        binding.btnResetBrightness.visibility = View.GONE
    }

    private fun setScreenRotationOnStart() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val rotation = prefs[getString(R.string.selected_orientation_key), 0]
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val returnValue = Settings.System.canWrite(this)
            if (returnValue) {
                setScreenRotation(rotation)
            }
        } else {
            setScreenRotation(rotation)
        }
    }

    private fun setScreenRotation(rotation: Int) {
        when (rotation) {
            0 -> requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
            1 -> requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
            2 -> requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            3 -> {
                Settings.System.putInt(contentResolver, Settings.System.ACCELEROMETER_ROTATION, 1)
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT
            }
            4 -> requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
            5 -> requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE
        }
    }


    private fun restoreSystemAutoRotation(autorotation: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val returnValue = Settings.System.canWrite(this)
            if (returnValue) {
                Settings.System.putInt(contentResolver, Settings.System.ACCELEROMETER_ROTATION, autorotation)
            }
        } else {
            Settings.System.putInt(contentResolver, Settings.System.ACCELEROMETER_ROTATION, autorotation)
        }
    }

    private fun restoreDefaultStateForButtons() {
        binding.llFab1Menu.translationY = 0f
        binding.llFab2Menu.translationY = 0f
        binding.llFab3Menu.translationY = 0f
        binding.llFab4Menu.translationY = 0f
        binding.llFab1Menu.visibility = View.GONE
        binding.llFab2Menu.visibility = View.GONE
        binding.llFab3Menu.visibility = View.GONE
        binding.llFab4Menu.visibility = View.GONE
        binding.fabMain.hide()
        binding.fabMain.rotation = 0f
        binding.fabMain.setImageResource(R.drawable.ic_edit_24dp)
        visibleFABMenu = false
        visibleButtons = false
    }

    private fun showDate(isDateVisible: Boolean) =
            if (isDateVisible) tv_date.visibility = View.VISIBLE else tv_date.visibility = View.INVISIBLE

    private fun setDate(currentDateFormat: String): String =
            formatTimeAndDate("EEEE\n" + currentDateFormat)

    private fun showSeconds(isSecondsVisible: Boolean) {
        if (isSecondsVisible) {
            iv_c1.visibility = View.VISIBLE
            iv_s0.visibility = View.VISIBLE
            iv_s1.visibility = View.VISIBLE
        } else {
            iv_c1.visibility = View.GONE
            iv_s0.visibility = View.GONE
            iv_s1.visibility = View.GONE
        }
    }

    private fun scaleClockSize(scale: Float) {
        with(lt_dc_base) {
            pivotX = 0f
            pivotY = 0f
            scaleX = scale
            scaleY = scale
        }
    }

    fun moveClockScreenSaver(scale: Float) {
        val viewX = lt_dc_base.x
        val viewY = lt_dc_base.y
        val isLandscape = this.resources.getBoolean(R.bool.is_landscape)
        val movingClockHeight = if (isLandscape) {
            lt_dc_base.height * scale + tv_date.height
        } else {
            lt_dc_base.height * scale + tv_date.height + tv_next_alarm.height
        }
        val x1 = 0f
        val x2 = (binding.root.width - lt_dc_base.width * scale) / 2f
        val x3 = binding.root.width - lt_dc_base.width * scale
        val y1 = resources.getDimension(R.dimen.dp16)
        val y2 = binding.root.height / 3f - movingClockHeight / 2f
        val y3 = (binding.root.height - movingClockHeight) / 2f
        val y4 = 2 * binding.root.height / 3f - movingClockHeight / 2f
        val y5 = binding.root.height - movingClockHeight
//         2 | 10 | 15
//         5 | 13 |  3
//         8 |  1 |  6
//        11 |  4 |  9
//        14 |  7 | 12
        fun setXY(x: Float, y: Float) {
            lt_dc_base.x = x
            lt_dc_base.y = y
        }
        when {
            viewX == x2 && viewY == y3 -> setXY(x1, y1) // 2
            viewX == x1 && viewY == y1 -> setXY(x3, y2) // 3
            viewX == x3 && viewY == y2 -> setXY(x2, y4) // 4
            viewX == x2 && viewY == y4 -> setXY(x1, y2) // 5
            viewX == x1 && viewY == y2 -> setXY(x3, y3) // 6
            viewX == x3 && viewY == y3 -> setXY(x2, y5) // 7
            viewX == x2 && viewY == y5 -> setXY(x1, y3) // 8
            viewX == x1 && viewY == y3 -> setXY(x3, y4) // 9
            viewX == x3 && viewY == y4 -> setXY(x2, y1) // 10
            viewX == x2 && viewY == y1 -> setXY(x1, y4) // 11
            viewX == x1 && viewY == y4 -> setXY(x3, y5) // 12
            viewX == x3 && viewY == y5 -> setXY(x2, y2) // 13
            viewX == x2 && viewY == y2 -> setXY(x1, y5) // 14
            viewX == x1 && viewY == y5 -> setXY(x3, y1) // 15
            else -> setXY(x2, y3) // 1
        }
    }

    private fun showAlarms(isAlarmVisible: Boolean) =
            if (isAlarmVisible) tv_next_alarm.visibility = View.VISIBLE else tv_next_alarm.visibility = View.INVISIBLE

    private fun openAlarms() {
        val intent = Intent(AlarmClock.ACTION_SHOW_ALARMS)
        startActivity(intent)
    }

    private fun setFullScreenFlags() {
        window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                View.SYSTEM_UI_FLAG_FULLSCREEN or
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
    }

    private fun cancelFullScreenFlags() {
        window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
    }

    private fun getStartY(): Float {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val isLandscape = this.resources.getBoolean(R.bool.is_landscape)
        val oldStartY = this.resources.getDimension(R.dimen.dp16)
        return if (isLandscape) {
            prefs[getString(R.string.pref_start_y_land), oldStartY]
        } else {
            prefs[getString(R.string.pref_start_y_port), oldStartY]
        }
    }

    private fun setStartY(newY: Float) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val isLandscape = this.resources.getBoolean(R.bool.is_landscape)
        if (isLandscape) {
            prefs.edit { put(getString(R.string.pref_start_y_land) to newY) }
        } else {
            prefs.edit { put(getString(R.string.pref_start_y_port) to newY) }
        }
    }

    private fun setDayMode() {
        binding.tvFab1Menu.text = getString(R.string.switch_to_night_mode)
        binding.fab1.setImageResource(R.drawable.ic_moon_24dp)
        binding.fab1.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.colorPurpleDark))
        setBackgroundClockColor(backgroundColorDay)
        setForegroundClockColor(foregroundColorDay)
        setDateColor(foregroundColorDay)
        setAlarmColor(foregroundColorDay)
        binding.toolbar.visibility = View.INVISIBLE
        binding.vSemiTransBack.visibility = View.INVISIBLE
        restoreDefaultStateForButtons()
        isDayMode = true
    }

    private fun setNightMode() {
        binding.tvFab1Menu.text = getString(R.string.switch_to_day_mode)
        binding.fab1.setImageResource(R.drawable.ic_sun_24dp)
        binding.fab1.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.colorDaySky))
        setBackgroundClockColor(backgroundColorNight)
        setForegroundClockColor(foregroundColorNight)
        setDateColor(foregroundColorNight)
        setAlarmColor(foregroundColorNight)
        binding.toolbar.visibility = View.INVISIBLE
        binding.vSemiTransBack.visibility = View.INVISIBLE
        restoreDefaultStateForButtons()
        isDayMode = false
    }

    private fun setBackgroundClockColor(color: String) {
        binding.root.setBackgroundColor(Color.parseColor(color))
    }

    private fun setForegroundClockColor(color: String) {
        iv_h0.setColorFilter(Color.parseColor(color))
        iv_h1.setColorFilter(Color.parseColor(color))
        iv_c0.setColorFilter(Color.parseColor(color))
        iv_m0.setColorFilter(Color.parseColor(color))
        iv_m1.setColorFilter(Color.parseColor(color))
        iv_c1.setColorFilter(Color.parseColor(color))
        iv_s0.setColorFilter(Color.parseColor(color))
        iv_s1.setColorFilter(Color.parseColor(color))
        tv_am_pm.setTextColor(Color.parseColor(color))
    }

    private fun setDateColor(color: String) {
        tv_date.setTextColor(Color.parseColor(color))
    }

    private fun setAlarmColor(color: String) {
        tv_next_alarm.setTextColor(Color.parseColor(color))
        setTextViewDrawableColor(tv_next_alarm, color)
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun openAndroidPermissionMenu(requestCode: Int) {
        val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS)
        intent.data = Uri.parse("package:" + applicationContext.packageName)
        startActivityForResult(intent, requestCode)
    }

    @TargetApi(Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            codeWriteSettingsPermissionBrightness -> if (Settings.System.canWrite(this)) {
                Toast.makeText(this, getString(R.string.ask_permission_success), Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, getString(R.string.ask_permission_fail), Toast.LENGTH_LONG).show()
            }
            codeWriteSettingsPermissionOrientation -> if (Settings.System.canWrite(this)) {
                Toast.makeText(this, getString(R.string.ask_permission_success), Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, getString(R.string.ask_orientation_permission_fail), Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun showAskForPermissionDialog(context: String) {
        when (context) {
            "brightness" -> AlertDialog.Builder(this)
                    .setCancelable(false) // prevent from dismissing dialog by clicking outside or back button which cosed getting out of fullscreen
                    .setTitle(getString(R.string.ask_permission_title))
                    .setMessage(getString(R.string.ask_permission_message))
                    .setPositiveButton(getString(R.string.ask_permission_positive)) { _, _ -> openAndroidPermissionMenu(codeWriteSettingsPermissionBrightness) }
                    .setNegativeButton(getString(R.string.ask_permission_negative)) { dialog, _ ->
                        dialog.dismiss()
                        setFullScreenOrNot(this)
                        Toast.makeText(this, getString(R.string.ask_permission_fail), Toast.LENGTH_LONG).show()
                    }
                    .create()
                    .show()
            "screen orientation" -> AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setTitle(getString(R.string.ask_permission_title))
                    .setMessage(getString(R.string.ask_orientation_permission))
                    .setPositiveButton(getString(R.string.ask_permission_positive)) { _, _ -> openAndroidPermissionMenu(codeWriteSettingsPermissionOrientation) }
                    .setNegativeButton(getString(R.string.ask_permission_negative)) { dialog, _ ->
                        dialog.dismiss()
                        setFullScreenOrNot(this)
                        Toast.makeText(this, getString(R.string.ask_orientation_permission_fail), Toast.LENGTH_LONG).show()
                    }
                    .create()
                    .show()
        }
    }

    private fun showOrientationSelectionDialog() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val orientations = arrayOf(getString(R.string.orient_system), getString(R.string.orient_auto), getString(R.string.orient_portrait), getString(R.string.orient_portrait_revers), getString(R.string.orient_land), getString(R.string.orient_land_revers))
        val checkedItem = prefs[getString(R.string.selected_orientation_key), 0]
        var selectedIndex = checkedItem

        AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(getString(R.string.select_screen_orientation))
                .setSingleChoiceItems(orientations, checkedItem) { _, which ->
                    selectedIndex = which
                }
                .setPositiveButton(getString(R.string.action_save)) { _, _ ->
                    saveOrientationSetting(selectedIndex)
                    setFullScreenOrNot(this)
                }
                .setNegativeButton(getString(R.string.ask_permission_negative)) { dialog, _ ->
                    dialog.dismiss()
                    setFullScreenOrNot(this)
                }
                .create()
                .show()
    }

    private fun saveOrientationSetting(which: Int) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        setScreenRotation(which)
        prefs.edit { put(getString(R.string.selected_orientation_key) to which) }
    }

    private fun moveDCUp() {
        val isLandscape = this.resources.getBoolean(R.bool.is_landscape)
        val topMargin = if (isLandscape) R.dimen.dp64 else R.dimen.dp72
        val rightMargin = if (isLandscape) {
            binding.root.width / 2 + resources.getDimension(R.dimen.dp8).toInt()
        } else {
            resources.getDimension(R.dimen.dp16).toInt()
        }
        scaleClockSize(1.0f)
        lt_dc_base.x = 0f
        lt_dc_base.animate().y(resources.getDimension(topMargin)).duration = 180
        lt_dc_base.setPadding(
                resources.getDimension(R.dimen.dp8).toInt(),
                resources.getDimension(R.dimen.dp8).toInt(),
                resources.getDimension(R.dimen.dp8).toInt(),
                resources.getDimension(R.dimen.dp8).toInt()
        )
        val params = lt_dc_base.layoutParams as ViewGroup.MarginLayoutParams
        params.setMargins(
                resources.getDimension(R.dimen.dp16).toInt(),
                resources.getDimension(R.dimen.dp16).toInt(),
                rightMargin,
                resources.getDimension(R.dimen.dp16).toInt()
        )
        lt_dc_base.layoutParams = params
        ViewCompat.setElevation(lt_dc_base, resources.getDimension(R.dimen.dp4))

    }


    private fun moveDCDown() {
        val sidePadding = resources.getDimension(R.dimen.dp16).toInt()
        lt_dc_base.animate().y(getStartY())
        lt_dc_base.setPadding(sidePadding, 0, sidePadding, 0)
        val params = lt_dc_base.layoutParams as ViewGroup.MarginLayoutParams
        params.setMargins(0, 0, 0, 0)
        lt_dc_base.layoutParams = params
        ViewCompat.setElevation(lt_dc_base, 0f)
    }

    private fun setDCEditorBackground() {
        binding.root.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBackgroundMain))
    }

    private fun resetDCEditorBackground() {
        lt_dc_base.setBackgroundColor(Color.TRANSPARENT)
        if (isDayMode) {
            binding.root.setBackgroundColor(Color.parseColor(backgroundColorDay))
        } else {
            binding.root.setBackgroundColor(Color.parseColor(backgroundColorNight))
        }
    }

    private fun hideDateAndAlarm() {
        tv_date.visibility = View.GONE
        tv_next_alarm.visibility = View.GONE
    }

    private fun showDateAndAlarm() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val isDateVisible = prefs[getString(R.string.pref_display_date_key), resources.getBoolean(R.bool.pref_display_date_default)]
        val isAlarmVisible = prefs[getString(R.string.pref_display_alarms_key), resources.getBoolean(R.bool.pref_display_alarms_default)]
        showDate(isDateVisible)
        showAlarms(isAlarmVisible)
    }

    private fun addDCThemeEditorFragment() {
        supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, editorFragment)
                .commit()
    }

    private fun removeDCThemeEditorFragment() {
        supportFragmentManager
                .beginTransaction()
                .remove(editorFragment)
                .commit()
    }

    private fun addOwnColorFragment() {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, ownColorFragment)
                .commit()
        val toolbarTitleString = if (ownColorEditType == "backcolor") {
            resources.getString(R.string.background_color)
        } else {
            resources.getString(R.string.foreground_color)
        }
        changeToolbarTitle(toolbarTitleString)
    }

    private fun removeOwnColorFragment() {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, editorFragment)
                .commit()
        isOwnColorEdited = false
        if (isDayMode) {
            changeToolbarTitle(getString(R.string.day_mode_settings))
        } else {
            changeToolbarTitle(getString(R.string.night_mode_settings))
        }

    }

    private fun changeToolbarTitle(title: String) {
        binding.toolbar.title = title
    }

    private fun setToolbarIconsThemeEdit() {
        binding.toolbar.setNavigationIcon(R.drawable.ic_close_24dp)
        binding.toolbar.setNavigationOnClickListener {
            restoreDCForegroundPreviewColor()
            restoreDCBackgroundPreviewColor()
            if (isOwnColorEdited) {
                removeOwnColorFragment()
            } else {
                hideDCThemeEditor()
                restoreThemePreview()
            }
        }
        invalidateOptionsMenu()
    }

    private fun removeToolbarIconsEdit() {
        binding.toolbar.navigationIcon = null
        invalidateOptionsMenu()
    }


    private fun showDCThemeEditor() {
        moveDCUp()
        if (isDayMode) {
            setForegroundClockColor(foregroundColorDayPreview)
            setDCThemeEditBackColorPreview(backgroundColorDayPreview)
            changeToolbarTitle(getString(R.string.day_mode_settings))
        } else {
            setForegroundClockColor(foregroundColorNightPreview)
            setDCThemeEditBackColorPreview(backgroundColorNightPreview)
            changeToolbarTitle(getString(R.string.night_mode_settings))
        }
        hideDateAndAlarm()
        setDCEditorBackground()
        setToolbarIconsThemeEdit()
        addDCThemeEditorFragment()
        binding.vSemiTransBack.visibility = View.INVISIBLE
        restoreDefaultStateForButtons()
        lt_dc_base.setOnTouchListener(null)
        isDCThemeEdited = true
    }

    private fun hideDCThemeEditor() {

        moveDCDown()
        if (isDayMode) {
            setForegroundClockColor(foregroundColorDay)
        } else {
            setForegroundClockColor(foregroundColorNight)
        }
        showDateAndAlarm()
        resetDCEditorBackground()
        removeDCThemeEditorFragment()
        changeToolbarTitle(getString(R.string.app_name))
        removeToolbarIconsEdit()
        binding.toolbar.visibility = View.INVISIBLE
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val isScreenSaver = prefs[getString(R.string.pref_screen_saver_key), resources.getBoolean(R.bool.pref_screen_saver_default)]
        if (!isScreenSaver) lt_dc_base.setOnTouchListener(this) else scaleClockSize(screenSaverClockScale)
        isDCThemeEdited = false
    }


    private fun setDCThemeEditBackColorPreview(backColor: String) {
        lt_dc_base.setBackgroundColor(Color.parseColor(backColor))
    }

    private fun saveDCForegroundColor() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        if (isDayMode) {
            prefs.edit { put(getString(R.string.pref_day_mode_fore_color) to foregroundColorDayPreview) }
        } else {
            prefs.edit { put(getString(R.string.pref_night_mode_fore_color) to foregroundColorNightPreview) }
        }
    }

    private fun saveDCBackgroundColor() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        if (isDayMode) {
            prefs.edit { put(getString(R.string.pref_day_mode_back_color) to backgroundColorDayPreview) }
        } else {
            prefs.edit { put(getString(R.string.pref_night_mode_back_color) to backgroundColorNightPreview) }
        }
    }

    private fun saveDCTheme() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        if (isDayMode) {
            prefs.edit { put(getString(R.string.pref_day_theme) to themeDayPreview) }
        } else {
            prefs.edit { put(getString(R.string.pref_night_theme) to themeNightPreview) }
        }
    }

    private fun restoreDCForegroundPreviewColor() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        if (isDayMode) {
            foregroundColorDayPreview = foregroundColorDay
            prefs.edit { put(getString(R.string.day_mode_fore_color_preview_key) to foregroundColorDayPreview) }
        } else {
            foregroundColorNightPreview = foregroundColorNight
            prefs.edit { put(getString(R.string.night_mode_fore_color_preview_key) to foregroundColorNightPreview) }
        }
    }

    private fun restoreDCBackgroundPreviewColor() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        if (isDayMode) {
            backgroundColorDayPreview = backgroundColorDay
            prefs.edit { put(getString(R.string.day_mode_back_color_preview_key) to backgroundColorDayPreview) }
        } else {
            backgroundColorNightPreview = backgroundColorNight
            prefs.edit { put(getString(R.string.night_mode_back_color_preview_key) to backgroundColorNightPreview) }
        }

    }

    private fun restoreThemePreview() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        if (isDayMode) {
            themeDayPreview = selectedThemeDay
            prefs.edit { put(getString(R.string.pref_day_theme_preview) to themeDayPreview) }
        } else {
            themeNightPreview = selectedThemeNight
            prefs.edit { put(getString(R.string.pref_night_theme_preview) to themeNightPreview) }
        }
    }


}
