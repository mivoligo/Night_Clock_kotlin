package org.mivoligo.nightclock


data class DCThemes(val preview: Int, val themeName: String)

fun getDCThemes(): List<DCThemes> {
    return listOf(
            DCThemes(R.drawable.dc_v1_preview, "dcV1"),
            DCThemes(R.drawable.dc_v2_preview, "dcV2"),
            DCThemes(R.drawable.dc_v3_preview, "dcV3"),
            DCThemes(R.drawable.dc_v4_preview, "dcV4"),
            DCThemes(R.drawable.dc_v5_preview, "dcV5"),
            DCThemes(R.drawable.dc_v6_preview, "dcV6"),
            DCThemes(R.drawable.dc_v7_preview, "dcV7")
    )
}

